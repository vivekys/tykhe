#!/usr/bin/env python

import logging
import numpy as np
import pandas as pd
import os
import torch
from tykhe.network.atcn.network import ETNT
from tykhe import util
from tykhe.stateManager import StateManager
from mrmarketgym.env.mrmarket import MrMarket
from dateutil.relativedelta import relativedelta

def runModel(model, env, date):
    n_digits = 3
    obs = env.reset(date)
    model.stateManager.reset(date)
    rewards = []
    cum_rewards = 0
    model.eval()
    with torch.no_grad():
        while True:
            normal = model([obs])
            changeInAction = normal.mean
            changeInAction = torch.round(changeInAction * 10 ** n_digits) / (10 ** n_digits)
            pvm = torch.FloatTensor(obs[1]).to(model.device)
            zero = torch.tensor(0.0).to(model.device)
            newAction = pvm.view(1, -1) + changeInAction
            newAction = torch.clamp(newAction, min=0.0, max=1.0)
            total = newAction.sum()
            newAction = newAction / total if not torch.equal(total, zero) else pvm
            changeInAction = newAction - pvm
            changeInAction_n = changeInAction.cpu().numpy()
            obs, reward, done, _ = env.step(changeInAction_n[0])
            rewards.append(reward)
            cum_rewards += reward
            if done:
                break
    print(f"Cumulative for the year {date} is {cum_rewards}")

def createAndLoadETNT(embeddingSize, latentFeatures, numLayers, kernel_size, dropout, stateManager, device = "cpu"):
    num_channels = [latentFeatures] * numLayers
    etnt = ETNT(embeddingSize, num_channels, kernel_size, dropout, stateManager,
                device=device).to(device)
    etnt.load()
    return etnt

def baseRunner(depth=30, combined = False, device = "cpu", start = "2002-04-01", end = "2013-03-31"):
    stateConfig = util.createStateConfig()
    depth = depth
    episodeLength = 1
    trainStart = pd.to_datetime(start, format='%Y-%m-%d')
    trainEnd = pd.to_datetime(end, format='%Y-%m-%d')
    oneDay = relativedelta(days=1)
    oneYear = relativedelta(years=1)
    validationStart = trainEnd + oneDay
    testStart = trainEnd - oneYear + oneDay

    stateManager = StateManager(stateConfig, depth, episodeLength, trainStart=start, trainEnd=end,
                                combined= combined, device = device)
    stateManager.reset(testStart)
    env = MrMarket(id=1, start_date=start,
                         episode_length=episodeLength,
                         max_drawdown=100,
                         commision=0.1,
                         start_cash=1000000,
                         add_cash=100000,
                         assets_cnt=len(stateConfig["assets"]) + 1,
                         m_assets=stateConfig["assets"],
                         metrics=True,
                         reporting=True)

    return stateManager, env


def run():
    device = "cpu"
    trainStart = "2002-04-01"
    trainEnd = "2015-03-31"
    validationStart = "2015-04-01"
    stateManager, env = baseRunner(combined=False, device=device, start=trainStart, end=trainEnd)
    model = createAndLoadETNT(embeddingSize=75, latentFeatures=75, numLayers=4, kernel_size=7, dropout=0.2,
                       stateManager=stateManager, device=device)
    runModel(model, env, validationStart)
    env.close()

if __name__ == '__main__':
    np.random.seed(42)
    torch.manual_seed(42)
    torch.backends.cudnn.deterministic = True
    torch.backends.cudnn.benchmark = False
    os.chdir("/Users/vivek/myCode/company/i18r/tykhe")
    log_format = '%(asctime)s,%(msecs)d %(levelname)-8s %(filename)s:%(lineno)d %(message)s'
    log_datefmt = '%Y-%m-%d:%H:%M:%S'
    logging.basicConfig(format=log_format, datefmt=log_datefmt, level=logging.ERROR)
    run()
