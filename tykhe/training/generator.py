#!/usr/bin/env python3

import pandas as pd
import numpy as np
import sklearn
from dateutil.relativedelta import relativedelta
import functools
import operator

class Generator:
    @staticmethod
    def getTrainingEpisodes(start, end, episodeLength=1):
        oneYear = relativedelta(years=episodeLength)
        start_t = pd.to_datetime(start, format='%Y-%m-%d')
        end_t = pd.to_datetime(end, format='%Y-%m-%d') - oneYear
        sample_days = [d.strftime('%Y-%m-%d') for d in pd.date_range(start=start_t, end=end_t, freq='D')]
        samples = sklearn.utils.shuffle(sample_days)
        return samples

# if __name__ == '__main__':
#     start = "2002-04-01"
#     end = "2012-03-31"
#     dates = Generator.getTrainingEpisodes(start, end)
#     print(dates)