#!/usr/bin/env python

from collections import namedtuple

Experience = namedtuple('Experience', ['state', 'action', 'reward', 'done', 'meta_info'])

class SimpleExperienceSource(object):
    def __init__(self, env, agent, startDate):
        self.env = env
        self.agent = agent
        self.startDate = startDate
        self.currentState = None
        self.nextState = None
        self.actions = None
        self.agentState = [None]

    def __iter__(self):
        while(True):
            if (self.currentState is None):
                self.currentState = self.env.reset(self.startDate)

            self.agentState = [None] * len(self.currentState)
            actions, self.agentState, meta_info = self.agent([self.currentState], self.agentState)
            changeInAction = meta_info["changeInAction"]
            self.nextState, reward, is_done, _ = self.env.step(changeInAction[0])
            exp = Experience(state=self.currentState, action=actions[0], reward=reward, done=is_done, meta_info=meta_info)
            self.currentState = self.nextState
            if (is_done is True):
                self.currentState = None
            yield exp

