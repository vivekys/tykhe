#!/usr/bin/env python

import logging
import math
import torch
import torch.nn as nn
from torch.nn.utils import weight_norm
import torch.nn.functional as F

"""
Remove initialization - Removing results in allocating to all assets equally which is a reasonable good start
"""
class Chomp1d(nn.Module):
    def __init__(self, chomp_size, device = "cpu"):
        super(Chomp1d, self).__init__()
        self.device = device
        self.chomp_size = chomp_size
        logging.info("Created Chomp1d Module")

    def forward(self, x):
        logging.debug(f"Chomp1d Input - {x.shape}")
        x = x
        output = x[:, :, :-self.chomp_size]
        logging.debug(f"Chomp1d Output - {output.shape}")
        return output


class TemporalBlock(nn.Module):
    def __init__(self, n_inputs, n_outputs, kernel_size, stride, dilation, padding, dropout=0.2, device = "cpu"):
        super(TemporalBlock, self).__init__()
        self.device = device
        self.conv1 = weight_norm(nn.Conv1d(n_inputs, n_outputs, kernel_size,
                                           stride=stride, padding=padding, dilation=dilation))
        self.chomp1 = Chomp1d(padding, device=self.device)
        self.elu1 = nn.ELU()
        self.dropout1 = nn.Dropout(dropout)

        self.conv2 = weight_norm(nn.Conv1d(n_outputs, n_outputs, kernel_size,
                                           stride=stride, padding=padding, dilation=dilation))
        self.chomp2 = Chomp1d(padding, device=self.device)
        self.elu2 = nn.ELU()
        self.dropout2 = nn.Dropout(dropout)

        self.net = nn.Sequential(self.conv1, self.chomp1, self.elu1, self.dropout1,
                                 self.conv2, self.chomp2, self.elu2, self.dropout2)
        self.downsample = nn.Conv1d(n_inputs, n_outputs, 1) if n_inputs != n_outputs else None
        self.downsample = self.downsample
        self.elu = nn.ELU()
        logging.info("Created TemporalBlock Module")

    def forward(self, x):
        logging.debug(f"TemporalBlock Input - {x.shape}")
        x = x
        out = self.net(x)
        res = x if self.downsample is None else self.downsample(x)
        output = self.elu(out + res)
        logging.debug(f"TemporalBlock Output - {output.shape}")
        return output


class AttentionBlock(nn.Module):
    """An attention mechanism similar to Vaswani et al (2017)
    The input of the AttentionBlock is `BxDxT` where `B` is the input
    minibatch size, `D` is the dimensions of each feature, `T` is the length of
    the sequence.
    The output of the AttentionBlock is `Bx(D+V)xT` where `V` is the size of the
    attention values.
    Arguments:
        input_dims (int): the number of dimensions (or channels) of each element
            in the input sequence
        k_size (int): the size of the attention keys
        v_size (int): the size of the attention values
    """
    def __init__(self, input_dims, k_size, v_size, device = "cpu"):
        super(AttentionBlock, self).__init__()
        self.device = device
        self.key_layer = weight_norm(nn.Linear(input_dims, k_size))
        self.query_layer = weight_norm(nn.Linear(input_dims, k_size))
        self.value_layer = weight_norm(nn.Linear(input_dims, v_size))
        self.sqrt_k = math.sqrt(k_size)

        logging.info("Created AttentionBlock Module")

    def forward(self, minibatch):
        logging.debug(f"AttentionBlock Input - {minibatch.shape}")
        minibatch = minibatch.permute(0,2,1)
        keys = self.key_layer(minibatch)
        queries = self.query_layer(minibatch)
        values = self.value_layer(minibatch)
        logits = torch.bmm(queries, keys.transpose(2,1))
        mask = logits.data.new(logits.size(1), logits.size(2)).fill_(1).byte()
        mask = torch.triu(mask, 1)
        mask = mask.unsqueeze(0).expand_as(logits)
        logits.data.masked_fill_(mask, float('-inf'))
        probs = F.softmax(logits / self.sqrt_k, dim=2)
        read = torch.bmm(probs, values)
        output = torch.cat([minibatch, read], dim=2).permute(0,2,1)
        logging.debug(f"AttentionBlock Output - {output.shape}")
        return output


class TemporalConvNet(nn.Module):
    def __init__(self, num_inputs, num_channels, kernel_size=2, dropout=0.2, device = "cpu"):
        super(TemporalConvNet, self).__init__()
        self.device = device
        layers = []
        num_levels = len(num_channels)
        for i in range(num_levels):
            dilation_size = 2 ** i
            in_channels = num_inputs if i == 0 else num_channels[i-1]
            out_channels = num_channels[i]
            layers += [TemporalBlock(in_channels, out_channels, kernel_size, stride=1, dilation=dilation_size,
                        padding=(kernel_size-1) * dilation_size, dropout=dropout, device=self.device)]

        self.network = nn.Sequential(*layers)
        logging.info("Created TemporalConvNet Module")

    def forward(self, x):
        logging.debug(f"TemporalConvNet Input - {x.shape}")
        output = self.network(x)
        logging.debug(f"TemporalConvNet Output - {output.shape}")
        return output

class TemporalAtnConvNet(nn.Module):
    def __init__(self, num_inputs, num_channels, kernel_size=2, dropout=0.2, device = "cpu"):
        super(TemporalAtnConvNet, self).__init__()
        self.device = device
        layers = []
        num_levels = len(num_channels)
        for i in range(num_levels):
            dilation_size = 2 ** i
            in_channels = num_inputs if i == 0 else num_channels[i-1]
            out_channels = num_channels[i]
            layers += [AttentionBlock(in_channels, in_channels, in_channels, device=self.device)]
            layers += [TemporalBlock(in_channels + in_channels, out_channels, kernel_size, stride=1,
                                     dilation=dilation_size, padding=(kernel_size-1) * dilation_size, dropout=dropout,
                                     device=self.device)]

        self.network = nn.Sequential(*layers)
        logging.info("Created TemporalAtnConvNet Module")

    def forward(self, x):
        logging.debug(f"TemporalAtnConvNet Input - {x.shape}")
        output = self.network(x)
        logging.debug(f"TemporalAtnConvNet Output - {output.shape}")
        return output
