#!/usr/bin/env python

import glob
import numpy as np
import logging
import torch
import torch.nn as nn
import torch.distributions as distributions
from torch.nn.utils import weight_norm

from tykhe.network.atcn.blocks import TemporalAtnConvNet
from tykhe.stateManager import StateManager
from tykhe.util import *

class TNT(nn.Module):
    def __init__(self, input_size, output_size, num_channels, kernel_size, dropout, device='cpu'):
        super(TNT, self).__init__()
        self.device = device
        self.tcn = TemporalAtnConvNet(input_size, num_channels, kernel_size=kernel_size, dropout=dropout,
                            device=self.device)
        self.linear = nn.Sequential(nn.Linear(num_channels[-1], output_size),
                                    nn.ELU(),
                                    nn.BatchNorm1d(output_size))
        logging.info("Created TNT Module")


    def forward(self, x):
        logging.debug(f"TNT Input - {x.shape}")
        y1 = self.tcn(x)
        output = self.linear(y1[:, :, -1])
        logging.debug(f"TNT Output - {output.shape}")
        return output

class ETNT(nn.Module):
    def __init__(self, hidden_size, num_channels, kernel_size, dropout, stateManager, device='cpu'):
        super(ETNT, self).__init__()
        self.hidden_size = hidden_size
        self.num_channels = num_channels
        self.kernel_size = kernel_size
        self.dropout = dropout
        self.stateManager = stateManager
        self.device = device
        self.actionCnt = len(self.stateManager.assets) + 1
        self.components = stateManager.components
        self.eLayers = nn.ModuleDict()
        for key, size in self.components.items():
            input_size = size
            tnt = nn.Sequential(
                TNT(input_size, self.hidden_size, self.num_channels, self.kernel_size, self.dropout, self.device)
            )
            self.eLayers.update({key: tnt})
        self.merger = nn.Sequential(
            nn.Linear(self.hidden_size * len(self.eLayers) + self.actionCnt, self.hidden_size),
            nn.ELU(),
            nn.BatchNorm1d(self.hidden_size),
            nn.Dropout(self.dropout)
        )
        self.mu = nn.Sequential(
            nn.Linear(self.hidden_size, self.actionCnt),
            nn.Tanh()
        )

        self.var = nn.Sequential(
            nn.Linear(self.hidden_size, self.actionCnt),
            nn.Sigmoid()
        )

        logging.info("Created ETNT Model")

    def transformStates(self, batchedTStates):
        batchedTFeatures = batchedTStates[0]
        batchedTPVM = batchedTStates[1]
        transformed = {}
        for name, batch in batchedTFeatures.items():
            t = batch.permute(0, 2, 1)
            transformed.update({name: t})
        batchedTPVM = batchedTPVM
        return (transformed, batchedTPVM)

    def forward(self, x):
        """
        :param x: batch of state identifiers
        :return: batch of mu and variance
        Input needs to be in the form of b, f, t
        """
        batchedTStates = self.stateManager.provideBatchedTensors(x)
        (features, pvm) = self.transformStates(batchedTStates)
        featureVector = []
        for name, tnt in self.eLayers.items():
            f = features.get(name)
            v = tnt(f)
            featureVector.append(v)

        featureVector.append(pvm)
        catFV = torch.cat(featureVector, dim=1)

        logging.debug(f"Concatenated Features - {catFV.shape}")
        merged = self.merger(catFV)
        mus = self.mu(merged)
        scaledMus = mus / 10
        vars = self.var(merged)
        scaledVars = vars / 2500
        scaledVars = scaledVars.clamp(min=0.000001)
        sigmas = torch.sqrt(scaledVars)
        normal = distributions.Normal(scaledMus, sigmas)
        return normal

    def save(self, name, path="saves/reinforce", isBest=True, state=None):
        if(isBest is True):
            year = self.stateManager.testEndDate.year
            month = self.stateManager.testEndDate.month
            storeDate = str(year) + "/" + str(month)
            os.makedirs(os.path.join(path, storeDate), exist_ok=True)
            name = name + ".pth"
            fname = os.path.join(path, storeDate, name)
        else:
            os.makedirs(os.path.join(path, "modelState"), exist_ok=True)
            fname = os.path.join(path, "modelState", name)
        modelState = {}
        modelState.update({"state_dict": self.state_dict()})
        if(state is not None):
            modelState.update({"state": state})
        logging.info(f"Saving ETNT Model at Path {fname}")
        torch.save(modelState, fname)

    def get_latest_file(self, fullpath):
        """Returns the name of the latest (most recent) file
        of the joined path(s)"""
        list_of_files = glob.iglob(fullpath)  # You may use iglob in Python3
        if not list_of_files:  # I prefer using the negation
            return None  # because it behaves like a shortcut
        latest_file = max(list_of_files, key=os.path.getctime, default=None)
        if latest_file is not None:
            _, filename = os.path.split(latest_file)
            return filename
        else:
            return None

    def load(self, path="saves/reinforce", name=None, isBest=True):
        if(isBest is True):
            year = self.stateManager.testEndDate.year
            month = self.stateManager.testEndDate.month
            storeDate = str(year) + "/" + str(month) + "/"
            if(name is None):
                name = self.get_latest_file(os.path.join(path, storeDate, "*.pth"))
                if(name is None):
                    logging.info(f"No Model to Load at {os.path.join(path, storeDate)}")
                    return
            fname = os.path.join(path, storeDate, name)
        else:
            name = self.get_latest_file(os.path.join(path, "*.pth"))
            if (name is None):
                logging.info(f"No Model to Load at {os.path.join(path)}")
                return
            fname = os.path.join(path, name)

        print(f"Loading ETNT Model from Path {fname}")
        modelState = torch.load(fname, map_location=self.device)
        state = modelState.get("state", None)
        state_dict = modelState.get("state_dict")
        self.load_state_dict(state_dict)
        return state

# if __name__ == '__main__':
#     os.chdir("/Users/vivek/myCode/company/i18r/tykhe")
#     logging.basicConfig(level=logging.ERROR)
#     stateConfig = createStateConfig(start="2002-04-01")
#     stateManager = StateManager(stateConfig, 30, 365, pca_combined = False)
#     num_channels = [64] * 4
#     etnt = ETNT(256, num_channels, 7, 0.2, stateManager, actor_critic=True, pca = True, dataType = "pca")
#     dummy_input = [("2002-06-01", np.array([1 /(len(stateManager.assets) + 1)] * (len(stateManager.assets) + 1))),
#                         ("2002-06-02", np.array([1 /(len(stateManager.assets) + 1)] * (len(stateManager.assets) + 1))),
#                         ("2002-06-03", np.array([1 /(len(stateManager.assets) + 1)] * (len(stateManager.assets) + 1)))]
#
#
#     print(etnt)
#     (means, vars, _) = etnt(dummy_input)
#     print(means)
#     print(vars)
#
#     etnt.save("2")
#
#     etnt_saved = ETNT(256, num_channels, 7, 0.2, stateManager, actor_critic=True, pca = True, dataType = "pca",
#                       incremental=True)
#
#     (means, vars, _) = etnt_saved(dummy_input)
#     print(means)
#     print(vars)
#
