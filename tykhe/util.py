#!/usr/bin/env python3

import os

def createDict(features, path):
    d = {}
    for f in features:
        d.update({f: path})
    return d

def createAssetSpecificFeatures(featurePrefix, specific_features, path):
    d = {}
    for f in specific_features:
        d.update({featurePrefix + "-" + f : path})
    return d

def createStateConfig():
    assets = {"NIFTY_50" : "data/01-Feb-2019/cleaned/indexData/NIFTY_50.csv",
#              "NIFTY_NEXT_50" : "data/01-Feb-2019/cleaned/indexData/NIFTY_NEXT_50.csv",
#              "NIFTY_500": "data/01-Feb-2019/cleaned/indexData/NIFTY_500.csv",
              "NIFTY_BANK": "data/01-Feb-2019/cleaned/indexData/NIFTY_BANK.csv",
#              "NIFTY_ENERGY": "data/01-Feb-2019/cleaned/indexData/NIFTY_ENERGY.csv",
#              "NIFTY_FMCG": "data/01-Feb-2019/cleaned/indexData/NIFTY_FMCG.csv",
              "NIFTY_IT": "data/01-Feb-2019/cleaned/indexData/NIFTY_IT.csv",
#              "NIFTY_MNC": "data/01-Feb-2019/cleaned/indexData/NIFTY_MNC.csv",
#              "NIFTY_PHARMA": "data/01-Feb-2019/cleaned/indexData/NIFTY_PHARMA.csv",
              "NIFTY_GS_10YR" : "data/01-Feb-2019/cleaned/indexData/NIFTY_GS_10YR.csv",
              "FD": "data/01-Feb-2019/cleaned/indexData/FD.csv"}

    common_features = ["adv_dec", "FII_Debt", "FII_Equity", "MF_Equity", "MF_Debt"]
    common_featureMap = createDict(common_features, "data/01-Feb-2019/cleaned/features/basic")

    derived_common_features = ["Brent_Oil_Futures-z", "Gold_Futures-z", "USD-yz", "EURO-yz", "GBP-yz", "YEN-yz",
                               "India_1-Year_Bond_Yield-yz", "India_10-Year_Bond_Yield-yz",
                               "China_10-Year_Bond_Yield-yz", "France_10-Year_Bond_Yield-yz",
                               "Germany_10-Year_Bond_Yield-yz", "Japan_10-Year_Bond_Yield-yz",
                               "United_Kingdom_10-Year_Bond_Yield-yz", "United_States_10-Year_Bond_Yield-yz",
                               "CAC_40-yz", "Dow_Jones_Industrial_Average-yz", "FTSE_100-yz", "NASDAQ_Composite-yz",
                               "Nikkei_225-yz", "S&P_500-yz", "Shanghai_Composite-yz"]

    derived_common_featureMap = createDict(derived_common_features, "data/01-Feb-2019/cleaned/features/derived/common")
    common_featureMap.update(derived_common_featureMap)

    derived_specific_features = ["z", "yz"]
    specific_features = ["dpc", "mpc", "qpc", "hypc", "ypc", "v", "mv", "qv", "hyv", "yv"]
    ti_features = ["adx", "adxr", "apo", "aroondown", "aroonosc", "aroonup", "atr", "dema", "dx", "fastd", "fastdRSI",
                   "fastk", "fastkRSI", "ht_trendline", "kama", "lowerband", "macd", "macdhist", "macdsignal",
                   "middleband", "minus_di", "minus_dm", "mom", "plus_di", "plus_dm", "rsi", "slowd", "slowk", "tema",
                   "trima", "upperband", "willr", "wma"]

    features = {}
    for a in assets:
        specific_features_map = createAssetSpecificFeatures(a + "_Close", specific_features,
                                                "data/01-Feb-2019/cleaned/features/derived/specific/indexData")
        specific_features_map.update(common_featureMap)
        if(a is not "NIFTY_GS_10YR" and a is not "FD"):
            derived_PE_features_map = createAssetSpecificFeatures(a + "_PE", derived_specific_features,
                                                    "data/01-Feb-2019/cleaned/features/derived/common")
            derived_PB_features_map = createAssetSpecificFeatures(a + "_PB", derived_specific_features,
                                                                  "data/01-Feb-2019/cleaned/features/derived/common")
            specific_features_map.update(derived_PE_features_map)
            specific_features_map.update(derived_PB_features_map)

        if(a is not "FD"):
            ti_specific_features_map = createAssetSpecificFeatures(a + "_Close", ti_features,
                                                "data/01-Feb-2019/cleaned/features/derived/specific/tiIndexData")
            specific_features_map.update(ti_specific_features_map)

        features.update({a: specific_features_map})

    stateConfig = dict()
    stateConfig.update({"assets": assets})
    stateConfig.update({"features": features})
    return stateConfig

# if __name__ == '__main__':
#     os.chdir("/Users/vivek/myCode/company/i18r/tykhe")
#     stateConfig = createStateConfig(start="2002-04-01")
#     print(stateConfig)