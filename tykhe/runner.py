#!/usr/bin/env python

import sys
import os
pwd = os.getcwd()
sys.path.append(pwd)

from tykhe import util
from tykhe.stateManager import StateManager
from tykhe.network.atcn.network import ETNT
from tykhe.algo.reinforce import ReinforceAlgorithm
from tykhe.algo.validator import *
from mrmarketgym.env.mrmarket import MrMarket


def createETNT(embeddingSize, latentFeatures, numLayers, kernel_size, dropout, stateManager, device = "cpu"):
    num_channels = [latentFeatures] * numLayers
    etnt = ETNT(embeddingSize, num_channels, kernel_size, dropout, stateManager,
                device=device).to(device)
    return etnt

def baseRunner(depth=30, combined = False, device = "cpu", start = "2002-04-01", end = "2013-03-31"):
    stateConfig = util.createStateConfig()
    depth = depth
    episodeLength = 1
    stateManager = StateManager(stateConfig, depth, episodeLength, trainStart=start, trainEnd=end,
                                combined= combined, device = device)
    train_env = MrMarket(id=1, start_date=start,
                         episode_length=episodeLength,
                         max_drawdown=100,
                         commision=0.5,
                         start_cash=1000000,
                         add_cash=100000,
                         assets_cnt=len(stateConfig["assets"]) + 1,
                         m_assets=stateConfig["assets"],
                         metrics=True)

    test_env = MrMarket(id=2, start_date=start,
                         episode_length=episodeLength,
                         max_drawdown=100,
                         commision=0.25,
                         start_cash=1000000,
                         add_cash=100000,
                         assets_cnt=len(stateConfig["assets"]) + 1,
                         m_assets=stateConfig["assets"],
                         metrics=True)

    validation_env = MrMarket(id=3, start_date=start,
                         episode_length=episodeLength,
                         max_drawdown=100,
                         commision=0.25,
                         start_cash=1000000,
                         add_cash=100000,
                         assets_cnt=len(stateConfig["assets"]) + 1,
                         m_assets=stateConfig["assets"],
                         metrics=True)

    return stateManager, train_env, test_env, validation_env

def runReinforce(learningRate, entropyBeta, gamma, model, trainEnv, testEnv, valEnv, trainStart, trainEnd, assetCount,
                 device = "cpu", isIncremental = True):
    reinforce = ReinforceAlgorithm(learningRate=learningRate, entropyBeta=entropyBeta,
                                   gamma=gamma, model=model.to(device), trainStart=trainStart, trainEnd=trainEnd,
                                   trainEnv=trainEnv, valEnv=valEnv, testEnv=testEnv, assetCount=assetCount,
                                   device=device, isIncremental=isIncremental)
    reinforce.train()

def run():
    device = "cuda"
    trainStart = "2002-04-01"
    trainEnd = "2012-03-31"
    stateManager, trainEnv, testEnv, valEnv = baseRunner(combined=False, device=device, start=trainStart, end=trainEnd)
    model = createETNT(embeddingSize=75, latentFeatures=75, numLayers=4, kernel_size=7, dropout=0.2,
                       stateManager=stateManager, device=device)

    runReinforce(learningRate=0.01, entropyBeta=0.01, gamma=0.9, model=model,
                 trainEnv=trainEnv, testEnv=testEnv, valEnv=valEnv, trainStart=trainStart, trainEnd=trainEnd,
                 assetCount=stateManager.assetCnt, device=device)


if __name__ == '__main__':
    np.random.seed(42)
    torch.manual_seed(42)
    torch.backends.cudnn.deterministic = True
    torch.backends.cudnn.benchmark = False
    os.chdir("/mnt/i18r/tykhe")
    log_format = '%(asctime)s,%(msecs)d %(levelname)-8s %(filename)s:%(lineno)d %(message)s'
    log_datefmt = '%Y-%m-%d:%H:%M:%S'
    logging.basicConfig(format=log_format, datefmt=log_datefmt, level=logging.ERROR)
    run()
