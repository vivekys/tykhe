#!/usr/bin/env python

from ptan.agent import BaseAgent

import logging
import torch
import numpy as np

class TykheAgent(BaseAgent):
    def __init__(self, model, actionCnt = 1):
        super(TykheAgent, self).__init__()
        self.model = model
        self.actionCnt = actionCnt
        logging.info("Created TykheAgent")

    def __call__(self, states, agent_states=None):
        """
        Return actions from given list of states
        :param states: list of states
        :return: list of actions
        """
        if agent_states is None:
            agent_states = [None] * len(states)

        self.model.eval()

        with torch.no_grad():
            normal = self.model(states)
            changeInAction = normal.sample().cpu().numpy()
            changeInAction = np.around(changeInAction, decimals=3)
            pvm = states[0][1]
            newAction = pvm.reshape(1, -1) + changeInAction
            newAction = np.clip(newAction, a_min=0, a_max=1)
            total = np.sum(newAction)
            newAction = newAction / total if total != 0 else pvm
            changeInAction = newAction - pvm

            meta_info = {}
            meta_info.update({"changeInAction": changeInAction})
            logging.debug(f"TykheAgent - actions shape - {newAction.shape}")
            return newAction, agent_states, meta_info
