#!/usr/bin/env python

import numpy as np
import time
import sys
from empyrical import annual_volatility, max_drawdown, sharpe_ratio

class PerformanceTracker:
    def __init__(self, writer):
        self.writer = writer

    def __enter__(self):
        return self

    def __exit__(self, *args):
        None

    def compute(self, dailyReturns, eIdx, validation = ""):
        dailyReturns = np.array(dailyReturns)
        av = annual_volatility(dailyReturns)
        cr = sum(dailyReturns)
        md = max_drawdown(dailyReturns)
        sr = sharpe_ratio(dailyReturns)
        days = len(dailyReturns)
        self.writer.add_scalar("Risk/annual_volatility" + validation, 0 if np.isnan(av) else av, eIdx)
        self.writer.add_scalar("Return/cum_returns" + validation, 0 if np.isnan(cr) else cr, eIdx)
        self.writer.add_scalar("Risk/max_drawdown" + validation, 0 if np.isnan(md) else md, eIdx)
        self.writer.add_scalar("Return/sharpe_ratio" + validation, 0 if np.isnan(sr) else sr, eIdx)
        self.writer.add_scalar("Return/days" + validation, days, eIdx)

class RewardTracker:
    def __init__(self, writer):
        self.writer = writer

    def __enter__(self):
        self.ts = time.time()
        self.ts_frame = 0
        self.total_rewards = []
        return self

    def __exit__(self, *args):
        self.writer.close()

    def rewards(self, rewards, frame, date=None, loss=None, lr=None, bestReward=None, avgReward=None):
        steps = len(rewards)
        cumRewards = np.sum(rewards)
        self.total_rewards.append(cumRewards)
        speed = (frame - self.ts_frame) / (time.time() - self.ts)
        self.ts_frame = frame
        self.ts = time.time()
        mean_reward = np.mean(self.total_rewards[-100:])
        date = "" if date is None else date
        loss = 0.0 if loss is None else loss
        lr = 0.0 if lr is None else lr
        print(f"E={len(self.total_rewards):{4}} "
              f"Y={date} "
              f"R={np.around(mean_reward, decimals=3):{7}} & {np.around(cumRewards, decimals=3):{7}} "
              f"BR={np.around(bestReward, decimals=3):{7}} & {np.around(avgReward, decimals=3):{7}} "
              f"S={steps:{3}} speed={speed:{2}.{2}} "
              f"LR={np.around(lr[0], decimals=5):{8}} "              
              f"L={np.around(loss, decimals=3)} ")
        sys.stdout.flush()
        self.writer.add_scalar("speed", speed, frame)
        self.writer.add_scalar("reward_100", mean_reward, frame)
        self.writer.add_scalar("reward", cumRewards, frame)
