#!/usr/bin/env python

import logging
import torch
import numpy as np
import time
from tykhe.algo.util import PerformanceTracker


def calc_qvals(rewards, gamma):
    res = []
    sum_r = 0.0
    for r in reversed(rewards):
        sum_r *= gamma
        sum_r += r
        res.append(sum_r)
    q_vals = list(reversed(res))
    q_vals = (q_vals - np.mean(q_vals))/ (np.std(q_vals) + 1e-2)
    return np.expand_dims(q_vals, axis=1)


def calc_logprob(normal, actions_v):
    if(type(normal) is list):
        pairsB = zip(normal, actions_v)
        logPB = torch.stack(list(map(lambda p: p[0].log_prob(p[1]), pairsB)))
    else:
        logPB = normal.log_prob(actions_v)

    logP = logPB
    logP = logP.squeeze()
    return logP

def validate_net(env, writer, model, validationCount, entropyBeta, gamma, date, metric):
    validationCount += 1
    ts = time.time()
    model.eval()
    cum_rewards = 0.0
    rewards = []
    steps = 0
    model.eval()
    times = 0
    normals = []
    actions = []
    n_digits = 3
    obs = env.reset(date)
    model.stateManager.reset(date)
    with torch.no_grad():
        with PerformanceTracker(writer) as pTracker:
            while True:
                e_time = time.time()
                normal = model([obs])
                times = times + time.time() - e_time
                changeInAction = normal.mean
                changeInAction = torch.round(changeInAction * 10 ** n_digits) / (10 ** n_digits)
                pvm = torch.FloatTensor(obs[1]).to(model.device)
                zero = torch.tensor(0.0).to(model.device)
                newAction = pvm.view(1, -1) + changeInAction
                newAction = torch.clamp(newAction, min=0.0, max=1.0)
                total = newAction.sum()
                newAction = newAction / total if not torch.equal(total, zero) else pvm
                changeInAction = newAction - pvm

                changeInAction_n = changeInAction.cpu().numpy()
                normals.append(normal)
                actions.append(changeInAction)
                obs, reward, done, _ = env.step(changeInAction_n[0])
                rewards.append(reward)
                cum_rewards += reward
                steps += 1
                if done:
                    rewards = rewards[1:]
                    rewards.append(0.0)
                    pTracker.compute(dailyReturns=rewards, eIdx=validationCount, validation="-v")
                    break


        batch_actions_t = actions
        log_prob = calc_logprob(normals, batch_actions_t)
        logging.debug(f"PG log_prob Shape - {log_prob.shape} at {validationCount}")

        batch_qvals = calc_qvals(rewards, gamma)
        batch_qvals_v = torch.tensor(np.array(batch_qvals), dtype=torch.float32)
        batch_q_v = batch_qvals_v
        logging.debug(f"PG batch_q_v Shape - {batch_q_v.shape} at {validationCount}")

        log_prob_v = -log_prob.to("cpu") * batch_q_v
        logging.debug(f"PG log_prob_v Shape - {log_prob_v.shape} at {validationCount}")

        loss_policy = log_prob_v.mean()

        entropy_loss_v = entropyBeta * (torch.tensor(list(map(lambda d: d.entropy().mean(), normals)),
                                                     dtype=torch.float32).mean())

        loss_v = loss_policy + entropy_loss_v

        print(f"{metric} Count={validationCount:{3}} "
              f"Y={date} "
              f"R={np.around(cum_rewards, decimals=3):{7}} "
              f"S={steps:{3}} "
              f"BR={np.around(model.stateManager.futMean.max(), decimals=3):{7}} & "
              f"{np.around(model.stateManager.futMean.mean(), decimals=3):{7}} "
              f"L={np.around(loss_v, decimals=3)} ")

        writer.add_scalar(metric + "/log_prob", log_prob.sum(), validationCount)
        writer.add_scalar(metric + "/batch_qvals_v", batch_qvals_v.sum(), validationCount)
        writer.add_scalar(metric + "/loss_entropy", entropy_loss_v, validationCount)
        writer.add_scalar(metric + "/loss_policy", loss_policy, validationCount)
        writer.add_scalar(metric + "/loss_total", loss_v, validationCount)
        writer.add_scalar(metric + "/reward", cum_rewards, validationCount)
        writer.add_scalar(metric + "/steps", steps, validationCount)

        return validationCount, cum_rewards, loss_v, steps
