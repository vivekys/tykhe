#!/usr/bin/env python

import numpy as np
import pandas as pd
import time
import logging
import torch
import torch.optim as optim
import torch.nn.utils as nn_utils
import torch.optim.lr_scheduler as lr_scheduler
from tensorboardX import SummaryWriter
from tykhe.agent.agents import TykheAgent
from tykhe.algo.util import PerformanceTracker
from tykhe.algo.util import RewardTracker
from tykhe.algo import validator
from tykhe.training.simpleExperience import SimpleExperienceSource
from tykhe.training.generator import Generator
from dateutil.relativedelta import relativedelta
import collections

class ReinforceAlgorithm:
    def __init__(self, learningRate, entropyBeta, gamma, model,
                 trainStart, trainEnd, trainEnv, valEnv, testEnv, assetCount,
                 device="cpu", TEST_AFTER_TRAIN = 25, annealingCycle = 25, isIncremental = False):
        self.learningRate = learningRate
        self.entropyBeta = entropyBeta
        self.gamma = gamma
        self.model = model
        self.device = device
        self.TEST_AFTER_TRAIN = TEST_AFTER_TRAIN
        self.optimizer = optim.Adam(self.model.parameters(), lr=self.learningRate, amsgrad=True)#, weight_decay=9e-1)
        self.scheduler = lr_scheduler.CosineAnnealingLR(self.optimizer, annealingCycle, self.learningRate / 100)

        self.trainStart = pd.to_datetime(trainStart, format='%Y-%m-%d')
        self.trainEnd = pd.to_datetime(trainEnd, format='%Y-%m-%d')
        self.oneDay = relativedelta(days=1)
        self.oneYear = relativedelta(years=1)
        self.validationStart = self.trainEnd + self.oneDay
        self.testStart = self.trainEnd - self.oneYear + self.oneDay
        self.trainDates = None

        self.trainEnv = trainEnv
        self.testEnv = testEnv
        self.valEnv = valEnv
        self.assetCount = assetCount

        self.epoch = 0
        self.testCount = 0
        self.valCount = 0
        self.trainCount = 0
        self.best_reward = 0
        self.episode_count = 0
#        self.CLIP_GRAD = 5.0
        self.testLoss = []
        self.trainLoss = []
        self.valLoss = []
        self.isIncremental = isIncremental
        self.writer = SummaryWriter(log_dir="runs/Reinforce-Tykhe")

        logging.info("Created ReinforceAlgorithm")

    def isSolved(self):
        return False

    def trainBatch(self, batch_states, batch_actions, batch_qvals):
        start_time = time.time()
        self.trainCount = self.trainCount + 1
        self.model.train()
        self.optimizer.zero_grad()
        states_v = batch_states
        batch_actions_t = torch.FloatTensor(batch_actions).to(self.device)
        batch_qvals_v = torch.FloatTensor(batch_qvals).to(self.device)

        logging.debug(f"Reinforce states_v Shape - {len(states_v)} at {self.trainCount}")
        logging.debug(f"Reinforce batch_actions_t Shape - {batch_actions_t.shape} at {self.trainCount}")
        logging.debug(f"Reinforce batch_qvals_v Shape - {batch_qvals_v.shape} at {self.trainCount}")

        normal = self.model(states_v)
        logging.debug(f"PG mu_v Shape - {(normal.batch_shape, normal.event_shape)} at {self.trainCount}")

        log_prob = validator.calc_logprob(normal, batch_actions_t)
        logging.debug(f"PG log_prob Shape - {log_prob.shape} at {self.trainCount}")

        batch_q_v = batch_qvals_v
        logging.debug(f"PG batch_q_v Shape - {batch_q_v.shape} at {self.trainCount}")

        log_prob_v = log_prob * batch_q_v
        logging.debug(f"PG log_prob_v Shape - {log_prob_v.shape} at {self.trainCount}")
        loss_policy = -1 * log_prob_v.mean()

        entropy_loss_v = -1 * self.entropyBeta * (normal.entropy().mean())

        loss_v = loss_policy #+ entropy_loss_v
        loss_v.backward()
#        nn_utils.clip_grad_norm_(self.model.parameters(), self.CLIP_GRAD)
        self.optimizer.step()
        self.scheduler.step()
        lr_rate = self.scheduler.get_lr()

        self.writer.add_scalar("model/log_prob", log_prob.sum(), self.trainCount)
        self.writer.add_scalar("model/batch_qvals_v", batch_qvals_v.sum(), self.trainCount)
        self.writer.add_scalar("model/loss_entropy", entropy_loss_v, self.trainCount)
        self.writer.add_scalar("model/loss_policy", loss_policy, self.trainCount)
        self.writer.add_scalar("model/loss_total", loss_v, self.trainCount)
        self.trainLoss.append(loss_v.item())
        self.writer.add_scalar("model/avg_loss", np.mean(self.trainLoss), self.trainCount)

        grad_max = 0.0
        weight_max = 0.0
        grad_means = 0.0
        weight_means = 0.0
        grad_count = 0
        parameterCount = 0
        zeros = 0
        for p in self.model.parameters():
            grad_max = max(grad_max, p.grad.abs().max().item())
            weight_max = max(weight_max, p.data.abs().max().item())
            grad_means += (p.grad ** 2).mean().sqrt().item()
            weight_means += (p.data ** 2).mean().sqrt().item()
            grad_count += 1
            pCount = p.numel()
            parameterCount += pCount
            zeros += pCount - p.nonzero().size(0)

        self.writer.add_scalar("gradient/grad_l2", grad_means / grad_count, self.trainCount)
        self.writer.add_scalar("gradient/weight_mean", weight_means / grad_count, self.trainCount)
        self.writer.add_scalar("gradient/grad_max", grad_max, self.trainCount)
        self.writer.add_scalar("gradient/weight_max", weight_max, self.trainCount)
        self.writer.add_scalar("gradient/parameter_count", parameterCount, self.trainCount)
        self.writer.add_scalar("gradient/zeros_count", zeros, self.trainCount)
        return lr_rate, loss_v.item()

    def trainOnExpSource(self, exp_source):
        batch_states, batch_actions, batch_qvals, batch_ch_actions = [], [], [], []
        cur_states, cur_actions, cur_rewards, cur_ch_actions = [], [], [], []
        for step_idx, exp in enumerate(exp_source):
            cur_states.append(exp.state)
            cur_actions.append(exp.action)
            cur_rewards.append(exp.reward)
            cur_ch_actions.append(exp.meta_info["changeInAction"])
            if exp.done is True:
                self.episode_count += 1
                cur_rewards = cur_rewards[1:]
                cur_rewards.append(0.0)
                batch_states.extend(cur_states)
                batch_actions.extend(cur_actions)
                batch_qvals.extend(validator.calc_qvals(cur_rewards, self.gamma))
                batch_ch_actions.extend(cur_ch_actions)

                if (self.device is "cuda"):
                    torch.cuda.empty_cache()
                (lr_rate, loss_v) = self.trainBatch(batch_states, batch_ch_actions, batch_qvals)
                if (self.device is "cuda"):
                    torch.cuda.empty_cache()
                break
        return cur_rewards, lr_rate, loss_v

    def train(self):
        if(self.isIncremental):
            self.loadModelState()

        with PerformanceTracker(self.writer) as pTracker:
            with RewardTracker(self.writer) as tracker:
                while(self.isSolved() is False):
                    ts = time.time()
                    self.epoch = self.epoch + 1
                    while self.trainDates is not None and len(self.trainDates) > 0:
                        trainD = self.trainDates.popleft()
                        self.model.stateManager.reset(trainD)
                        self.tykheAgent = TykheAgent(self.model, actionCnt=self.assetCount)
                        self.tykheExpSource = SimpleExperienceSource(self.trainEnv, self.tykheAgent, trainD)
                        (cur_rewards, lr_rate, loss_v) = self.trainOnExpSource(self.tykheExpSource)
                        self.writer.add_scalar("episode/steps", len(cur_rewards), self.episode_count)
                        tracker.rewards(cur_rewards, self.episode_count, date=trainD, loss=loss_v, lr=lr_rate,
                                        bestReward=self.model.stateManager.futMean.max(),
                                        avgReward=self.model.stateManager.futMean.mean())
                        pTracker.compute(cur_rewards, self.episode_count)
                        if(self.episode_count % self.TEST_AFTER_TRAIN == 0):
                            (testCount, cum_rewards, loss, steps) = validator.validate_net(self.testEnv,
                                                    self.writer, self.model, self.testCount, self.entropyBeta,
                                                    self.gamma, self.testStart.strftime('%Y-%m-%d'), "test")
                            self.testCount = testCount
                            self.testLoss.append(loss)
                            self.writer.add_scalar("test/avg_loss", np.mean(self.testLoss), self.testCount)

                            isBest = False
                            if self.best_reward < cum_rewards:
                                if steps >= 364:
                                    print("Best reward updated: %.3f -> %.3f" % (self.best_reward, cum_rewards))
                                    self.best_reward = cum_rewards
                                    isBest = True

                            self.saveBestModel(isBest)

                            (valCount, cum_rewards, loss, steps) = validator.validate_net(self.valEnv,
                                                                                          self.writer,
                                                                                          self.model,
                                                                                          self.valCount,
                                                                                          self.entropyBeta,
                                                                                          self.gamma,
                                                                                          self.validationStart.strftime(
                                                                                              '%Y-%m-%d'),
                                                                                          "validation")
                            self.valCount = valCount
                            self.valLoss.append(loss)
                            self.writer.add_scalar("validation/avg_loss", np.mean(self.valLoss), self.valCount)


                    (valCount, cum_rewards, loss, steps) = validator.validate_net(self.valEnv, self.writer,
                                                self.model, self.valCount, self.entropyBeta, self.gamma,
                                                self.validationStart.strftime('%Y-%m-%d'), "validation")

                    self.valCount = valCount
                    self.valLoss.append(loss)
                    self.writer.add_scalar("validation/avg_loss", np.mean(self.valLoss), self.valCount)

                    self.trainDates = collections.deque(Generator.getTrainingEpisodes(self.trainStart, self.trainEnd))
                    print(f"Completed epoch {self.epoch} with avg test loss {np.mean(self.testLoss)} "
                          f"and avg val loss {np.mean(self.valLoss)} in time {time.time() - ts}")
                    logging.debug(f"Training Dates {self.trainDates}")

    def saveBestModel(self, isBest):
        if(isBest):
            name = "reinforce_%+.3f_%d" % (self.best_reward, self.trainCount)
        else:
            name = "reinforce_" + self.trainEnd.strftime('%Y-%m-%d')

        self.model.save(name=name, isBest=isBest, state=self.trainDates)

    def loadModelState(self):
        self.trainDates = self.model.load(path="saves/reinforce/modelState", isBest=False)
