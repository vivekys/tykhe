#!/usr/bin/env python

import sys
import os
pwd = os.getcwd()
sys.path.append(pwd)

import numpy as np
import pandas as pd
from sklearn.decomposition import PCA
import torch
from joblib import Parallel, delayed
from dateutil.relativedelta import relativedelta
import os
import logging
from tykhe.util import *
"""
StateManager is a utility class that provides states for a given stateIdentifier.

StateIdentifier - Tuple(timeId, positionVector)
                        - timeId represent the datetime for which the state is requested for.
                        - positionVector is the portfolio holding at timeId - 1

StateManager is constructed from stateConfig and depth

stateConfig - Dict
    "assets" : {"assetName" : "Path"}
    "features": { "assetName" : {"featureName" : "path"}}
    "startDate" : "Date"
"""
class StateManager:
    def __init__(self, stateConfig, depth, episodeLength_Y, trainStart, trainEnd, combined = False, device ="cpu",
                 timeEndDate ="2019-01-31", timeFutureDate = "2019-01-31"):
        self.stateConfig = stateConfig
        self.timeStart = "2002-01-01"
        self.timeStartD = pd.to_datetime(self.timeStart, format='%Y-%m-%d')

        self.timeEndDate = timeEndDate
        self.timeEndD = pd.to_datetime(self.timeEndDate, format='%Y-%m-%d')

        self.timeFutureDate = timeFutureDate
        self.timeFutureD = pd.to_datetime(self.timeFutureDate, format='%Y-%m-%d')

        self.depth = depth
        self.episodeLength_Y = episodeLength_Y
        self.trainStart = trainStart
        self.trainEnd = trainEnd
        self.combined = combined
        self.assets = stateConfig["assets"]
        self.assetCnt = len(self.assets) + 1
        self.featureMap = stateConfig["features"]
        self.device = device
        self.pcaMap = {}
        self.components = {}
        self.scalerMap = {}
        self.completeDataMap = self.getCompleteDataMap()
        self.reset(self.trainStart)
        logging.info("Initialized StateManager")

    def reset(self, start_date):
        self.startDate = start_date
        self.start = pd.to_datetime(self.startDate, format='%Y-%m-%d')
        self.one = relativedelta(days=1)
        episode = relativedelta(years=self.episodeLength_Y)
        self.endDate = self.start + episode - self.one
        self.episodeLength = (self.endDate - self.start).days + 1

        self.beginDate = pd.to_datetime(self.startDate, format='%Y-%m-%d') - relativedelta(days=self.depth) - self.one
        self.endDate = pd.to_datetime(self.startDate, format='%Y-%m-%d') + relativedelta(days=self.episodeLength) - self.one
        self.testEndDate = pd.to_datetime(self.startDate, format='%Y-%m-%d') + relativedelta(days=self.episodeLength) + \
                           relativedelta(months=1) - self.one

        self.annualVMap = None
        self.__initializeVMap()
        self.annualRetMap = None
        self.__initializeAnnualRetMap()
        self.annualRetData = None
        self.__initializeAnnualRetData()
        self.annualVData = None
        self.__initializeVData()

        self.futMean = None
        self.futVar = None
        self.__initializeMeanVar()

        dataMap, dataTMap = self.getDataMap()
        self.dataMap = dataMap
        self.dataTMap = dataTMap
        pvm = self.__initializePVM()
        self.pvm = pvm
        logging.info(f"Completed Reset of StateManager for the date {self.start}")

    def __initializeVData(self):
        dfs = []
        for toLoad, path in self.annualVMap.items():
            name, df = self.loader(toLoad, path)
            dfs.append(df)
        data = pd.concat(dfs, axis=1, sort=True)
        self.annualVData = data.reindex(sorted(data.columns), axis=1)

    def __initializeVMap(self):
        annualVMap = {}
        for asset in self.assets:
            annualVMap.update({asset + "_Close-v": "data/01-Feb-2019/cleaned/features/derived/specific/indexData"})
        self.annualVMap = annualVMap

    def __initializeMeanVar(self):
        self.futMean = self.annualRetData.loc[self.endDate]
        self.futVar = self.annualVData.loc[self.endDate]

    def __initializeAnnualRetData(self):
        dfs = []
        for toLoad, path in self.annualRetMap.items():
            name, df = self.loader(toLoad, path)
            dfs.append(df)
        data = pd.concat(dfs, axis=1, sort=True)
        self.annualRetData = data.reindex(sorted(data.columns), axis=1)

    def __initializeAnnualRetMap(self):
        annualRetMap = {}
        for asset in self.assets:
            annualRetMap.update({asset + "_Close-ypc": "data/01-Feb-2019/cleaned/features/derived/specific/indexData"})
        self.annualRetMap = annualRetMap

    def __initializePVM(self):
        index = pd.date_range(start=self.beginDate, end=self.testEndDate, freq="D")
        length = index.shape[0]
        data = np.full((length, self.assetCnt), 1 / self.assetCnt)
        df = pd.DataFrame(data=data, index=index)
        logging.info("Initialized PVM")
        return torch.tensor(df.values, dtype=torch.float32).to(self.device)


    def getCompleteDataMap(self):
        featureMapData = Parallel(n_jobs=8, prefer='processes')\
            (delayed(self.dataLoader)(features, assetName) for (assetName, features) in self.featureMap.items())
        featureMapData = dict(featureMapData)
        dataMap = {}
        components = {}
        features = {}
        index = pd.date_range(start=self.timeStartD, end=self.timeFutureD, freq="D")
        for name in self.assets:
            featureDFData = featureMapData[name]
            scalerDFDict = dict(featureDFData[1])
            self.scalerMap.update(scalerDFDict)
            featureDFList = featureDFData[0]
            featureDF = list(map(lambda f: f[1], featureDFList))
            featureDF = list(map(lambda df: df.fillna(self.scalerMap[df.columns[0]][0]), featureDF))
            #Scaling
            featureDF = list(map(lambda df:
                                (df - self.scalerMap[df.columns[0]][0])/self.scalerMap[df.columns[0]][1], featureDF))
            if(self.combined is False):
                finalDF = pd.concat(featureDF, sort=True, axis=1)
                pca = PCA(n_components=0.9)
                pca.fit(finalDF.loc[:self.trainEnd])
                data = pca.transform(finalDF)
                pca_df = pd.DataFrame(data, index=index)
                self.pcaMap.update({name: pca})
                dataMap.update({name: pca_df})
                components.update({name: pca.components_.shape[0]})
            else:
                for fDF in featureDF:
                    features.update({fDF.columns[0]: fDF})

        if(self.combined is True):
            combinedDf = pd.concat(list(features.values()), sort=True, axis=1)
            pca = PCA(n_components=0.9)
            pca.fit(combinedDf.loc[:self.trainEnd])
            data = pca.transform(combinedDf)
            pca_df = pd.DataFrame(data, index=index)
            self.pcaMap.update({"components": pca})
            dataMap.update({"components": pca_df})
            components.update({"components": pca.components_.shape[0]})

        self.components = components

        logging.info("Initialized CompleteDataMap ")
        return dataMap

    def getDataMap(self):
        dataMap = {}
        dataTMap = {}

        for name, df in self.completeDataMap.items():
            df = df.loc[self.beginDate:self.testEndDate]
            dataMap.update({name: df})
            dataTMap.update({name: torch.tensor(df.values, dtype=torch.float32).to(self.device)})

        logging.info("Initialized DataMaps")
        return dataMap, dataTMap


    def getScaler(self, name, path):
        prePath = path.split("features/")[0]
        postPath = path.split("features/")[1]
        scalerPath = prePath + "features/scaler/" + postPath
        df = self.loader(name, scalerPath)[1].loc[self.trainEnd]
        mean = df.iloc[0]
        std = df.iloc[1]
        return (name, (mean, std))

    def loader(self, name, path):
        file = path + "/" + name + ".csv"
        df = pd.read_csv(file, float_precision="high")
        df['Date'] = pd.to_datetime(df['Date'], format='%Y-%m-%d')
        df = df.set_index("Date")
        idx = pd.date_range(start=self.timeStartD, end=self.timeFutureD, freq="D")
        df = df.reindex(idx, fill_value=np.NaN)
        df = df.loc[self.timeStartD:]
        return (name, df)

    def dataLoader(self, toLoad, aName=None):
        logging.info("Loading Data to StateManager")
        res = Parallel(n_jobs=8, prefer='processes')(
            delayed(self.loader)(name, path) for (name, path) in toLoad.items())

        scaler = Parallel(n_jobs=8, prefer='processes')(
            delayed(self.getScaler)(name, path) for (name, path) in toLoad.items())

        if (aName != None):
            return (aName, (res, scaler))
        return (res, scaler)

    def provide(self, stateIdentifier, pvmHistory=False, tensorData = False):
        stateMap = {}
        self.updatePVM(stateIdentifier)
        currentDate = pd.to_datetime(stateIdentifier[0], format='%Y-%m-%d') - self.one
        nowNum = (currentDate - self.beginDate).days
        delta = relativedelta(days=self.depth)
        pastDate = currentDate - delta
        pastNum = (pastDate - self.beginDate).days
        if(tensorData == False):
            data = self.dataMap
        else:
            data = self.dataTMap

        for name, df in data.items():
            if(type(df) == pd.core.frame.DataFrame):
                d = df.iloc[pastNum:nowNum]
                logging.debug(f"{name} df shape : {d.shape}")
                stateMap.update({name: d})
            else:
                t = df[pastNum:nowNum]
                logging.debug(f"{name} df shape : {t.shape}")
                stateMap.update({name: t})
        posVector = self.pvm[nowNum] if pvmHistory == False else self.pvm[pastNum:nowNum]
        return (stateMap, posVector)

    def provideBatched(self, stateIdentifiers, pvmHistory=False):
        batchStates = {}
        batchPVM = []
        for stateIdentifier in stateIdentifiers:
            data = self.provide(stateIdentifier, pvmHistory=pvmHistory)
            features = data[0]
            posVector = data[1]
            for name, df in features.items():
                f = batchStates.get(name, [])
                f.append(df)
                batchStates.update({name: f})
            batchPVM.append(posVector)
        return (batchStates, batchPVM)

    def provideBatchedTensors(self, stateIdentifiers, pvmHistory=False):
        batchStates = {}
        batchPVM = []
        for stateIdentifier in stateIdentifiers:
            data = self.provide(stateIdentifier, pvmHistory=pvmHistory, tensorData=True)
            features = data[0]
            posVector = data[1]
            for name, df in features.items():
                f = batchStates.get(name, [])
                f.append(df)
                batchStates.update({name: f})
            batchPVM.append(posVector)
        transformedFeatures = {}
        for name, t in batchStates.items():
            transformedFeatures.update({name: torch.stack(t)})
        transformedPVM = torch.stack(batchPVM)
        return (transformedFeatures, transformedPVM)

    def updatePVM(self, stateIdentifier):
        currentDate = pd.to_datetime(stateIdentifier[0], format='%Y-%m-%d')
        currentNum = (currentDate - self.beginDate).days - 1
        posVector = stateIdentifier[1]
        self.pvm[currentNum] = torch.tensor(posVector, dtype=torch.float32).to(self.device)

    def dump(self):
        return self.dataMap

    def getPVM(self):
        return self.pvm



# if __name__ == '__main__':
#     os.chdir("/Users/vivek/myCode/company/i18r/tykhe")
#
#     trainStart = "2002-04-01"
#     trainEnd = "2012-03-31"
#     scaled = True
#     startDate = "2011-04-01"
#     depth = 30
#     episodeLength = 1
#
#     stateConfig = createStateConfig()
#     stateManager = StateManager(stateConfig, depth, episodeLength, trainStart=trainStart, trainEnd=trainEnd,
#                                 combined = False)
#     assetCnt = stateManager.assetCnt
#     stateManager.reset(startDate)
#
#     # res = stateManager.provide(("2012-06-01", np.array([1/assetCnt] * assetCnt)))
#     #
#     # batchedRes1 = stateManager.provideBatched([("2012-06-01", np.array([1/assetCnt] * assetCnt))])
#     #
#     # batchedRes2 = stateManager.provideBatched([("2012-04-01", np.array([1/assetCnt] * assetCnt)),
#     #                                     ("2012-06-02", np.array([1/assetCnt] * assetCnt)),
#     #                                     ("2012-06-03", np.array([1/assetCnt] * assetCnt))])
#     #
#     # batchedTRes1 = stateManager.provideBatchedTensors([("2012-04-01", np.array([1/assetCnt] * assetCnt)),
#     #                                     ("2012-06-02", np.array([1/assetCnt] * assetCnt)),
#     #                                     ("2012-06-03", np.array([1/assetCnt] * assetCnt))])
#     #
#     # batchedTRes2 = stateManager.provideBatchedTensors([("2012-06-01", np.array([1/assetCnt] * assetCnt)),
#     #                                     ("2012-06-02", np.array([1/assetCnt] * assetCnt)),
#     #                                     ("2012-06-03", np.array([1/assetCnt] * assetCnt))])
#     #
#     # batchedTRes3 = stateManager.provideBatchedTensors([("2012-06-01", np.array([1/assetCnt] * assetCnt)),
#     #                                     ("2012-06-02", np.array([1/assetCnt] * assetCnt)),
#     #                                     ("2012-06-03", np.array([1/assetCnt] * assetCnt))])
#     #
#     #
#     # res = stateManager.provide(("2012-06-01", np.array([1/assetCnt] * assetCnt)))
#     # print(res)
#     # res = stateManager.provide(("2012-06-01", np.array([1/assetCnt] * assetCnt)), tensorData=True)
#     # print(res)
#     print(sorted(stateManager.assets))
#     print(stateManager.futMean.sort_index())
#     print(stateManager.futVar.sort_index())
#     print(stateManager.components)
#     print(f"Avg Returns {stateManager.futMean.mean()}")
#     print(f"Max Returns {stateManager.futMean.max()}")
#     None